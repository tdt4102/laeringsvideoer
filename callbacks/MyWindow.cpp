#include "MyWindow.h"

MyWindow::MyWindow(int x, int y, int w, int h, const string& title) :
    AnimationWindow(x, y, w, h, title),
    btn1{100, 100, 70, 20, "Button 1"},
    btn2{100, 175, 70, 20, "Button 2"},
    btn3{100, 250, 70, 20, "Button 3"},
    textOutput{200, 150, 130, 20, "Text"}
    {
        add(btn1);
        add(btn2);
        add(btn3);
        add(textOutput);

        btn1.callback(cb_btn1, this);
        btn2.callback(cb_btn2, this);
        btn3.callback(cb_btn3, this);
    }

void MyWindow::cb_btn1(Fl_Widget*, void* pw) {
    static_cast<MyWindow*>(pw)->btn1_pressed();
}

void MyWindow::cb_btn2(Fl_Widget*, void* pw) {
    static_cast<MyWindow*>(pw)->btn2_pressed();
}

void MyWindow::cb_btn3(Fl_Widget*, void* pw) {
    static_cast<MyWindow*>(pw)->btn3_pressed();
}

void MyWindow::btn1_pressed() {
    string text = "Button 1 pressed";
    textOutput.value(text.c_str());
}

void MyWindow::btn2_pressed() {
    string text = "Button 2 pressed";
    textOutput.value(text.c_str());
}

void MyWindow::btn3_pressed() {
    string text = "Button 3 pressed";
    textOutput.value(text.c_str());
}

 