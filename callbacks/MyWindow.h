#pragma once
#include "AnimationWindow.h"

class MyWindow : public AnimationWindow {
    public: 
        MyWindow(int x, int y, int w, int h, const string& title);
    private:
        Fl_Button btn1;
        Fl_Button btn2;
        Fl_Button btn3;
        Fl_Output textOutput;

        static void cb_btn1(Fl_Widget*, void* pw);
        static void cb_btn2(Fl_Widget*, void* pw);
        static void cb_btn3(Fl_Widget*, void* pw);

        void btn1_pressed();
        void btn2_pressed();
        void btn3_pressed();
};
