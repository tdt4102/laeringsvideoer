#include "std_lib_facilities.h"

int main() {
	map<string, int> score;
	score.insert({"player 1", 990});
	score.insert({"player 2", 1100});
	score.insert({"player 3", 820});

	cout << score.at("player 1");

	score["player 3"] = 1200;

	for (pair<string, int> el : score) { // kan også bruke auto
		cout << el.first << ":\t" << el.second << '\n'; 
	}
}
