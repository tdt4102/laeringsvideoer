#include "MyWindow.h"

MyWindow::MyWindow(int x, int y, int w, int h, const std::string& title) : 
    AnimationWindow(x, y, w, h, title),
    shapeType{fieldPad, pad, fieldW, fieldH, "Shape"},
    shapeSize{fieldPad, 2*pad, fieldW, fieldH, "Size"},
    shapeColor{fieldPad, 3*pad, fieldW, fieldH, "Color"},
    shapePositionx{fieldPad, 4*pad, fieldW, fieldH, "Point x"},
    shapePositiony{fieldPad, 5*pad, fieldW, fieldH, "Point y"},
    drawShapeBtn{fieldPad, pad*13/2, fieldW, fieldH, "Draw Shape"}
    {
        add(shapeType);
        add(shapeSize);
        add(shapeColor);
        add(shapePositionx);
        add(shapePositiony);
        add(drawShapeBtn);
        drawShapeBtn.callback(cb_draw_shape, this);
    }

void MyWindow::drawShape() {
    string type = shapeType.value();
    string ssize = shapeSize.value();
    string scolor = shapeColor.value();
    string sx = shapePositionx.value();
    string sy = shapePositiony.value();
    if (type.empty() || ssize.empty() || scolor.empty() || sx.empty() || sy.empty()) {
        draw_text(Point{fieldPad, 9*pad}, "A field cannot be empty", Color::red);
        return;
    }
    int size = stoi(ssize);
    Color color{stringToColor.at(scolor)};
    Point p{stoi(sx), stoi(sy)};

    
    if (type == "circle") {
        draw_circle(p, size, color);
    }
    else if (type == "rectangle" || type == "rect") {
        draw_rectangle(p, size, size, color);
    }

    
    shapeType.value("");
    shapeSize.value("");
    shapeColor.value("");
    shapePositionx.value("");
    shapePositiony.value("");

}

void MyWindow::cb_draw_shape(Fl_Widget*, void* pw) {
    static_cast<MyWindow*>(pw)->drawShape();
}