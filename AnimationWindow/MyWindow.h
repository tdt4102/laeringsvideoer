#pragma once 
#include "AnimationWindow.h"

class MyWindow : public AnimationWindow {
    public:
        MyWindow(int x, int y, int w, int h, const string& title);
    private:
        Fl_Input shapeType;
        Fl_Input shapeSize;
        Fl_Input shapeColor;
        Fl_Input shapePositionx;
        Fl_Input shapePositiony;
        Fl_Button drawShapeBtn;

        static constexpr int pad = 20;
        static constexpr int fieldPad = 60;
        static constexpr int fieldW = 100;
        static constexpr int fieldH = 20;

        static void cb_draw_shape(Fl_Widget*, void* pw);
        void drawShape();

};

const map<string, Color> stringToColor {
    {"red", Color::red},
    {"blue", Color::blue},
    {"green", Color::green},
    {"yellow", Color::yellow},
    {"white", Color::white},
    {"black", Color::black},
    {"magenta", Color::magenta},
    {"cyan", Color::cyan},
    {"dark_red", Color::dark_red},
    {"dark_green", Color::dark_green},
    {"dark_yellow", Color::yellow},
    {"dark_blue", Color::dark_blue},
    {"dark_magenta", Color::dark_magenta},
    {"dark_cyan", Color::dark_cyan},
    {"gray", Color::gray},
    {"mid_gray", Color::mid_gray},
    {"dark_gray", Color::dark_gray},
    {"light_gray", Color::light_gray}
};