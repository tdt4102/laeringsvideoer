#include "std_lib_facilities.h"

int main() {
	int num = 43;

    switch(num) {
        case 1: cout << "Num is 1"; break;
        case 2: cout << "Num is 2"; break;
        case 3: cout << "Num is 3"; break;
        case 4: cout << "Num is 4"; break;
        case 5: cout << "Num is 5"; break;
        case 6: cout << "Num is 6"; break;
        case 7: cout << "Num is 7"; break;
        case 8: cout << "Num is 8"; break;
        case 9: cout << "Num is 9"; break;
        case 10: cout << "Num is 10"; break;

        default:
            cout << "Num is not in the range 1-10";
    }
    cout << '\n';
}

