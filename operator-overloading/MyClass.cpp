#include "MyClass.h"


MyClass::MyClass(string name, int num): 
    name{name},
    number{num}
    {}


ostream& operator<<(ostream& os, const MyClass& mc) {
    os << "Name: " << mc.name << "\nNumber: " << mc.number << '\n';
    return os;
}

MyClass& MyClass::operator+=(const MyClass& rhs) {
    this->name += " " + rhs.name;
    this->number += rhs.number;
    return *this;
}

bool MyClass::operator==(const MyClass& rhs) {
    return this->name == rhs.name && this->number == rhs.number;
}