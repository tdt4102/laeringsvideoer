#pragma once
#include "std_lib_facilities.h"

class MyClass {
    public:
        MyClass(string name, int num);
        friend ostream& operator<<(ostream& os, const MyClass& mc);
        MyClass operator+(const MyClass& rhs) const;
        MyClass& operator+=(const MyClass& rhs);
        bool operator==(const MyClass& rhs);
    private:
        string name;
        int number;
};