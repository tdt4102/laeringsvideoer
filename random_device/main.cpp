#include "std_lib_facilities.h"


void rollDice(int n) {
	random_device rd;
	
	default_random_engine generator(rd());
	uniform_int_distribution<int> distribution(1,6);

	vector<int> dices;

	for (int i=0; i<n; i++){
		int dice = distribution(generator);
		dices.push_back(dice);
	}
	
	for (int j=0; j<n; j++) {
		cout << dices.at(j) << ",\n"[j == n-1];
	}
}

int main() {
	rollDice(5);
}