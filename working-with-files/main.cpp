#include "std_lib_facilities.h"


string readFromFile(string filename) {
    ifstream ifs{filename};

    if (!ifs) {
        error("Unable to open file: "+ filename+ '\n');
    }

    string out = "";
    char temp;

    while(ifs >> temp) {
        out += temp;
    }

    return out;
    
}

void writeToFile(string filename, string info) {
    ofstream ofs{filename};
    
    ofs << info;
}

int main() {
    cout << readFromFile("placeholder.txt");
}